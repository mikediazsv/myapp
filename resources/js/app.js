
require('./bootstrap');

window.Vue = require('vue').default;
import Vue from 'vue';
import VueRouter from 'vue-router'
Vue.use(VueRouter);

/*Vue.component('form-component', require('./components/Tasks/Form.vue').default);
Vue.component('list-component', require('./components/Tasks/Lists.vue').default);*/
import Form from './components/Tasks/Form.vue';
import Tasks from './components/Tasks/Lists.vue';
import Edit from './components/Tasks/Edit.vue';
const routes=[
    {
        path: '/form',
        component: Form,
    },
    {
        path: '/',
        component: Tasks,
    },
    {
        path: '/edit/:id',
        component: Edit,
    }

];

const router = new VueRouter({routes});


const app = new Vue({
    el: '#app',
    router:router
});
