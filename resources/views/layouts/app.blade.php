<!doctype html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tailwindcss/2.0.1/tailwind.min.css">
</head>
<body class="h-screen bg-gradient-to-b from-blue-200 to-blue-500" >
  
   

  <div id="app">
        @yield("content")
</div>
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>