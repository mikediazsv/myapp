@extends("layouts.app")

@section("content")
<nav class="relative w-full flex flex-wrap items-center  py-3 bg-gray-900 text-gray-200 shadow-lg navbar navbar-expand-lg navbar-light">
    
    <router-link to="/form"
    class="nav-link text-white mx-3 p-2 opacity-60 hover:opacity-80 focus:opacity-80 "
    active-class="font-bold"
    exact>Agregar Task
    </router-link>

    <router-link to="/"
    class="nnav-link text-white mx-3 p-2 opacity-60 hover:opacity-80 focus:opacity-80 "
    active-class="font-bold"
    exact>Listar Task
    </router-link>
</nav>

    <div class="flex justify-center mt-20">
        
        <router-view></router-view>

    </div>
@endsection